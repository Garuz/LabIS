(function () {
    var app = angular.module('starter.controllers', []);

    var cafeterias_URI = "http://eina.unizar.es/archivos/2014_2015/Menus%20cafeterias/";

    /**********************************************************************
     * AppCtrl: Controlador principal de la aplicación.
     ***********************************************************************/
    app.controller('AppCtrl', function ($scope, $timeout, $state) {

        // Timeout para ir a home
        $timeout(function () {
            $state.go('app.home');
        }, 2000);
    });

    app.controller('HomeCtrl', function ($scope, geoService, $ionicPopup, $window) {
        
        ///////////////////////

        $scope.pisos = [
            {id: 0, text: 'Sótano', checked: false, icon: null},
            {id: 1, text: 'Planta calle', checked: false, icon: null},
            {id: 2, text: 'Planta 1', checked: false, icon: null},
            {id: 3, text: 'Planta 2', checked: true, icon: null},
            {id: 4, text: 'Planta 3', checked: true, icon: null},
            {id: 5, text: 'Planta 4', checked: true, icon: null}];

        //$scope.pisos_texto = 'Elige planta';
        $scope.val =  {single: $scope.pisos[1].id};
        $scope.pisos_texto = $scope.pisos[1].text;

        ///////////////////////
        
        $scope.locateUser = function(){
            console.log("called");
            geoService.locateUser($scope);
        };
        
        $scope.placeFloor = function(piso){
            console.log(piso);
            geoService.placeFloor($scope,piso-1);
        };
        
        geoService.placeBaseMap($scope);
        geoService.placeFloor($scope,0);
        //geoService.placeBasePolygons($scope);
        //geoService.placeBaseAda($scope,0);
        //geoService.placeBaseTorres($scope,0);
        //geoService.placeBaseBetancourt($scope,0);


        /*----------------------------------------------*/
        /*----------- POPUP MENÚS CAFETERÍAS --------- */
        /*----------------------------------------------*/
        $scope.showPopupCafeterias = function() {
            $scope.cafeteriaPopup = $ionicPopup.show({
                templateUrl: 'templates/cafeteria-popup.html',
                scope: $scope
            });
        };

        $scope.closePopup = function(){ $scope.cafeteriaPopup.close();}

        $scope.downloadMenu = function(data){
            switch (data){
                case 1:
                    //cafeteriaService.downloadMenu("ADA_BYRON_2_8FEBRERO2015.pdf");
                    $window.open(cafeterias_URI + "ADA_BYRON_2_8FEBRERO2015.pdf", '_system');
                    $scope.cafeteriaPopup.close();
                    break;
                case 2:
                    $window.open(cafeterias_URI + "BETANCOURT_2_8FEBRERO2015.pdf", '_system');
                    $scope.cafeteriaPopup.close();
                    break;
                case 3:
                    $window.open(cafeterias_URI + "TORRES_QUEVEDO_2_8_FEBRERO2015.pdf", '_system');
                    $scope.cafeteriaPopup.close();
                    break;
            }
        }

    });

    /**************************************************************************
     * TopCtrl: Controlador encargado de redirigir la aplicación a la pantalla
     *          de splash en caso de refresco de página
     ***********************************************************************/
    app.controller('TopCtrl', function ($location) {
        $location.path("/");
    });

})();
