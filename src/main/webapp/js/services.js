(function () {
    var app = angular.module('starter.services', []);

    var URI_SERVICE = "http://localhost:8080/rest/reina/";

    app.service('geoService', function ($http) {

        var adaLayer = L.geoJson();
        var torresLayer = L.geoJson();
        var betanLayer = L.geoJson();

        return ({
            placeBaseMap: placeBaseMap,
            placeBasePolygons: placeBasePolygons,
            placeBaseAda: placeBaseAda,
            placeBaseTorres: placeBaseTorres,
            placeBaseBetancourt: placeBaseBetancourt,
            placeFloor: placeFloor,
            removeOverlays: removeOverlays,
            locateUser: locateUser
        });

        function placeBaseMap($scope) {

            //===========================//
            ////// LEAFLET INIT VARS //////
            //===========================//

            var MIN_ZOOM = 17;
            var INIT_ZOOM = 18;
            var MAX_ZOOM = 20;

            //Campus
            var MAP_LAT = 41.68337;
            var MAP_LON = -0.8883134;

            //===========================//
            ////// LEAFLET BASE MAP ///////
            //===========================//

            var OSM = L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                minZoom: MIN_ZOOM,
                maxZoom: MAX_ZOOM,
                id: 'OSM'
            });

            var Clean = L.tileLayer('http://{s}.tile.stamen.com/toner-lite/{z}/{x}/{y}.{ext}', {
                subdomains: 'abcd',
                minZoom: MIN_ZOOM,
                maxZoom: MAX_ZOOM,
                ext: 'png',
                id: 'Clean'
            });
            
            var terrainDayMobile = L.tileLayer('http://{s}.{base}.maps.cit.api.here.com/maptile/2.1/maptile/{mapID}/terrain.day.mobile/{z}/{x}/{y}/256/png8?app_id={app_id}&app_code={app_code}', {
                subdomains: '1234',
                mapID: 'newest',
                app_id: 'Y8m9dK2brESDPGJPdrvs',
                app_code: 'dq2MYIvjAotR8tHvY8Q_Dg',
                base: 'aerial',
                minZoom: MIN_ZOOM,
                maxZoom: MAX_ZOOM,
                id: 'TDM'
            });

            var worldStreetMap = L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}', {
                minZoom: MIN_ZOOM,
                maxZoom: MAX_ZOOM,
                id: 'WSM'
            });

            var worldTopoMap = L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer/tile/{z}/{y}/{x}', {
                minZoom: MIN_ZOOM,
                maxZoom: MAX_ZOOM,
                id: 'WTM'
            });

            //var mapBox = L.tileLayer('http://{s}.tiles.mapbox.com/v3/MapBox.' + your_api_code + '/{z}/{x}/{y}.png', {
            //    subdomains: 'abcd',
            //    minZoom: MIN_ZOOM,
            //    maxZoom: MAX_ZOOM,
            //    id: 'MapBox'
            //});

            var baseMaps = {
                "OSM": OSM,
                "Clean": Clean,
                "TDM": terrainDayMobile,
                "WSM": worldStreetMap,
                "WTM": worldTopoMap//,
                //"MapBox": mapBox
            };

            $scope.map = L.map('map-rioebro',
                {
                    zoomControl: false,
                    layers: [Clean]
                }).setView([MAP_LAT, MAP_LON], INIT_ZOOM);

            L.control.layers(baseMaps, {}, {position: 'bottomleft'}).addTo($scope.map);

            new L.Control.Zoom({position: 'bottomright'}).addTo($scope.map);

            $scope.map.attributionControl.setPrefix('');

            //===========================//
            ///////// WORLD LIMIT /////////
            //===========================//

            $scope.map.bounds = [],
                $scope.map.setMaxBounds([
                    [41.6867518, -0.890032],
                    [41.6800233, -0.8819532]
                ]);

            var bounds = new L.latLngBounds([41.6867518, -0.890032], [41.6800233, -0.8819532]);
            $scope.map.fitBounds(bounds);
        };

        function placeBasePolygons($scope) {
            $http.get("data/poligonosCampus.json")
                .success(function (data) {
                    L.geoJson(data, {
                        style: function (feature) {
                            return {weight: 1, color: "#565656", opacity: 0.50}
                        }
                    }).addTo($scope.map);
                });
        };

        function placeBaseAda($scope, floor) {
            $http.get("data/adaBase" + floor + ".json")
                .success(function (data) {
                    adaLayer.clearLayers();
                    adaLayer = L.geoJson(data, {
                        style: function (feature) {
                            return {weight: 1, color: "#18BFF2"}
                        },
                        onEachFeature: onEachFeature
                    });
                    adaLayer.addTo($scope.map);
                }).error(function (data) {
                    adaLayer.clearLayers();
                    adaLayer.addTo($scope.map);
                    console.log('no map');
                });
        };

        function placeBaseTorres($scope, floor) {
            $http.get("data/torresBase" + floor + ".json")
                .success(function (data) {
                    torresLayer.clearLayers();
                    torresLayer = L.geoJson(data, {
                        style: function (feature, floor) {
                            return {weight: 1, color: "#E81515"}
                        },
                        onEachFeature: onEachFeature
                    });
                    torresLayer.addTo($scope.map);
                }).error(function (data) {
                    torresLayer.clearLayers();
                    torresLayer.addTo($scope.map);
                    console.log('no map');
                });
        };

        function placeBaseBetancourt($scope, floor) {
            $http.get("data/betancourtBase" + floor + ".json")
                .success(function (data) {
                    betanLayer.clearLayers();
                    betanLayer = L.geoJson(data, {
                        style: function (feature) {
                            return {weight: 1, color: "#E67709"}
                        },
                        onEachFeature: onEachFeature
                    });
                    betanLayer.addTo($scope.map);
                }).error(function (data) {
                    betanLayer.clearLayers();
                    betanLayer.addTo($scope.map);
                    console.log('no map');
                });
        };

        function onEachFeature(feature, layer) {
            if (feature.properties && feature.properties.ID_CENTRO) {
                layer.bindPopup(feature.properties.ID_CENTRO);
                //layer.on('mouseover', function (e) {
                //    this.openPopup();
                //});
                //layer.on('mouseout', function (e) {
                //    this.closePopup();
                //});
            }
        };

        function placeFloor($scope, floor) {
            placeBaseAda($scope, floor);
            placeBaseTorres($scope, floor);
            placeBaseBetancourt($scope, floor);
        };

        function removeOverlays($scope) {
            adaLayer.clearLayers();
            torresLayer.clearLayers();
            betanLayer.clearLayers();
            adaLayer.addTo($scope.map);
            torresLayer.addTo($scope.map);
            betanLayer.addTo($scope.map);
        };
        
        function locateUser($scope){
            $scope.map.locate({setView: true, enableHighAccuracy: true, maxZoom: 20});
        };
    });
})();