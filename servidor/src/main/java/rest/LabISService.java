package rest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

import com.google.gson.Gson;

/**
 * 
 * Servicio RESTful para LabIS
 *
 */

@Path("/labis-service")
public class LabISService {
	
	/**
	 * Devuelve el listado de incidencias que aparecen en la p�gina web del Centro 
	 * Universitario de Lenguas Modernas.
	 * 
	 * @return lista de incidencias del CULM
	 */
	@Path("/buscar-clase")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String buscarClase(String edificio) {
		
		Gson gson = new Gson();
			
		try{
					
			String response = gson.toJson(null);
			return response;
			
		} catch (Exception e){
			System.err.println(e);
			return gson.toJson(null);
			
		}
	}
	
	/**
	 * ALGUN EJEMPLO REALIZADO
	 * 	/**
	 * Devuelve el listado de noticias que aparecen en la p�gina web del Centro 
	 * Universitario de Lenguas Modernas.
	 * 
	 * @return lista de noticias del CULM
	 *
	@Path("/noticias")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String getNoticias() {
		
		Scrapper scrapper = new Scrapper();
		Gson gson = new Gson();
		List<Noticia> noticias = null;
		List<String> noticiasError = null;
		
		try{
			noticias = scrapper.getNoticias();
				
			String response = gson.toJson(noticias);
			
			return response;
		
		} catch (IOException e){
			System.err.println(e);
			noticiasError = new ArrayList<String>();
			noticiasError.add("Jaunt Scrapping library Error");
			return gson.toJson(noticiasError);
			
		}
	}
	
	
	/**
	 * Devuelve el listado con los links a las convocatorias de Junio por curso y horario
	 * que aparecen en la p�gina web del Centro Universitario de Lenguas Modernas.
	 * 
	 * @return lista de convocatorias de Junio
	 *
	@Path("/examenesJunio")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String getExamenesJunio() {
		
		Scrapper scrapper = new Scrapper();
		Gson gson = new Gson();
		List<Examen> examenes = null;
		List<String> examenesError = null;
		
		try{
			examenes = scrapper.getExamenes(0);
				
			String response = gson.toJson(examenes);
			
			return response;
		
		} catch (IOException e){
			System.err.println(e);
			examenesError = new ArrayList<String>();
			examenesError.add("Jaunt Scrapping library Error");
			return gson.toJson(examenesError);
			
		}
	}
	 */

}
